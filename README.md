# AlmaCalc

This calculates delays for a radio interferometer using a version
of the venerable Calc 11 library that is adapted to the ALMA radio
telescope.

The single source tree has mechanisms for creating a Python module as well
as a CMake file for creating a shared library callable from Fortran and C/C++

## Python Tutorial

There are two interfaces:

  - A *high-level interface* based on Astropy 4.0+ (and Python 3.6+).
  - A *low-level interface* based on NumPy (even works on Python 2.7
    for some reason!).

The high-level interface is optional and its dependencies can be installed
via `pip install .[highlevel]`. It takes care of Earth orientation parameters
and uses Astropy classes like `EarthLocation`, `Time` and `SkyCoord`:

```Python
import astropy.units as u
from astropy.time import Time
from astropy.coordinates import EarthLocation, ICRS

from almacalc.highlevel import calc

array_reference = EarthLocation.from_geodetic('21:26:38.0', '-30:42:39.8', 1086.6)
ants = EarthLocation.from_geocentric([5109271.497, 5109748.526],
                                     [2006808.893, 2003331.232],
                                     [-3239130.736, -3240538.853], unit=u.m)
temperature = 20.0 * u.deg_C
pressure = 905. * u.hPa
humidity = 0.2
obstime = Time(['2020-11-03 16:33:00', '2020-11-03 16:33:10', '2020-11-03 16:33:20'])
source = ICRS('19:39:25.02671h', '-63:42:45.6255d')
delay = calc(ants, source, obstime, array_reference, temperature, pressure, humidity)
# Output shape is (3 timestamps, 2 antennas):
# <Quantity [[-4.02324797e-07, -4.98588047e-06],
#            [-4.02364853e-07, -4.98969785e-06],
#            [-4.02404978e-07, -4.99351509e-06]] s>
```

The low-level interface is quite a bit more verbose, but nevertheless
adds a few niceties like array broadcasting and basic workarounds to
the basement-level Fortran interface:

```Python
import numpy as np

from almacalc.lowlevel import calc

# Array reference position (ITRS), in metres
# NB: this can't be at the centre of the Earth for now
ref_x = 5109360.13332123
ref_y = 2006852.58604291
ref_z = -3238948.12747888
# The ITRS positions of two antennas, in metres
ant_x = [5109271.497, 5109748.526]
ant_y = [2006808.893, 2003331.232]
ant_z = [-3239130.736, -3240538.853]
# The weather at each antenna
temperature = [20.0, 20.0]  # degrees C
pressure = [905.0, 905.0]  # hPa / millibar
humidity = [0.2, 0.2]  # fraction between 0 and 1

# The times at which to evaluate delays, in Modified Julian Days UTC
# Three timestamps spaced 10 seconds apart, starting at 2020-11-03 16:33:00
mjd = 59156.68958333333 + np.arange(0, 30, 10) / 86400
# The ICRS coordinates of the source (e.g. J193925.0-634245), in radians
ra, dec = 5.146177964, -1.11199593219
# The Earth orientation parameters applicable to these times
dut1 = [-0.17292238, -0.17292234, -0.17292229]  # UT1 - UTC, seconds
pm_x = [0.15630551, 0.15630536, 0.15630521]  # x component of polar motion, arcsecs
pm_y = [0.29504742, 0.29504736, 0.29504729]  # y component of polar motion, arcsecs
# The difference between TAI and UTC (basically the total number of leap seconds)
dtai = 37.0

# Calculate geometric, dry and wet delays per time and per antenna
geo_delay, dry_delay, wet_delay = calc(ref_x, ref_y, ref_z, ant_x, ant_y, ant_z,
                                       temperature, pressure, humidity, mjd,
                                       ra, dec, pm_x, pm_y, dut1, dtai)
delay = geo_delay + dry_delay + wet_delay
# Output shape is (3 timestamps, 2 antennas):
# array([[-4.02324797e-07, -4.98588047e-06],
#        [-4.02364853e-07, -4.98969785e-06],
#        [-4.02404978e-07, -4.99351509e-06]])
```

## Creating and using the shared library

A shared library called calc11 can be created and installed (in /usr/local
by default) with

```
mkdir build
cd build
cmake ..
make install
```
Calling from fortran is obvious! However, there is a slightly modified routine using 
fortran 2003 standard
to make calling from C easier. A suitable header file ([ccalc.h](tests/ccalc.h))
with a function declaration is also installed in /usr/local/include.

You can run a simple test of an [example invocation of ccalc](tests/test_ccalc.C) with

```
make test ARGS="-V"
```

## History

This is version 11 of Goddard CALC provided by David Gordon in
July 2016.  ~~This version contains implementations of the
near field models of Sekido-Fukushima, Duev, and laser ranging
and native DiFX interface~~(they are missing or commented out).

It was further modified in 2019 by David Gordon and Ralph Marson
to suit the ALMA telescope. This version is included as the
AlmaCalc11 third-party package in the open-source [ALMA CONTROL
software repository](https://bitbucket.alma.cl/scm/asw/control.git).

Finally, in 2020 Ludwig Schwardt turned AlmaCalc11 into a Python
package based on earlier work by Mark Ashdown.

In 2021 the UVW calculation was reintroduced by un-commenting
and a routine ccalc added to make calling from C/C++ easier and more natural
by Paul Harrison.

