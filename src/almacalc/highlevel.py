"""High-level interface to Calc based on Astropy."""
import astropy.units as u
from astropy.utils import iers
import erfa

from . import lowlevel


@u.quantity_input(equivalencies=u.temperature())
def calc(antenna_locations, source, obstime, reference_location=None,
         temperature: u.deg_C = 0. * u.deg_C,
         pressure: u.hPa = 0. * u.hPa,
         relative_humidity: u.dimensionless_unscaled = 0.,
         axis_offset: u.m = 0. * u.m) -> u.s:
    """Calculate delays towards source between antennas and reference point.

    Parameters
    ----------
    antenna_locations : :class:`~astropy.coordinates.EarthLocation`, shape A
        ITRS location of each antenna
    source : :class:`~astropy.coordinates.ICRS`, shape () or T
        ICRS source position (or multiple positions, one per time slot)
    obstime : :class:`~astropy.time.Time`, shape T
        The times when to compute the delays
    reference_location : :class:`~astropy.coordinates.EarthLocation`, shape (), optional
        Location of array reference point (defaults to first antenna)
    temperature : :class:`~astropy.units.Quantity`, shape () or A, optional
        Ambient air temperature at each antenna
    pressure : :class:`~astropy.units.Quantity`, shape () or A, optional
        Total barometric pressure at each antenna
    relative_humidity : :class:`~astropy.units.Quantity` or float, shape () or A, optional
        Relative humidity at each antenna as dimensionless quantity between 0 to 1
    axis_offset : :class:`~astropy.units.Quantity`, shape () or A, optional
        Axis offsets for each antenna

    Returns
    -------
    delay : :class:`~astropy.units.Quantity`, shape T + A
        Total delay per time slot between each antenna and reference
    """
    original_ants_shape = antenna_locations.shape
    # Ensure that antenna locations are 1-dimensional (not scalars or multi-dimensional)
    ants = antenna_locations.ravel()
    ref = ants[0] if reference_location is None else reference_location

    # Convert antenna-related parameters to the appropriate units and shape (scalar or 1-dim)
    ref_x = ref.x.to_value(u.m).item()
    ref_y = ref.y.to_value(u.m).item()
    ref_z = ref.z.to_value(u.m).item()
    ants_x = ants.x.to_value(u.m)
    ants_y = ants.y.to_value(u.m)
    ants_z = ants.z.to_value(u.m)
    temperature = temperature.to_value(u.deg_C, equivalencies=u.temperature()).ravel()
    pressure = pressure.to_value(u.hPa).ravel()
    relative_humidity = (relative_humidity << u.dimensionless_unscaled).value.ravel()
    axis_offset = axis_offset.to_value(u.m).ravel()

    original_times_shape = obstime.shape
    # Ensure that observation times are 1-dimensional (not scalars or multi-dimensional)
    times = obstime.ravel()
    # Open the currently active IERS table to get Earth orientation parameters.
    # Override this with `iers.earth_orientation_table.set(new_table)` before calling calc.
    eop = iers.earth_orientation_table.get()
    delta_ut1_utc = eop.ut1_utc(times)
    polar_motion_x, polar_motion_y = eop.pm_xy(times)
    # Obtain TAI - UTC (= number of leap seconds) from ERFA
    t0 = times[0].ymdhms
    frac_day = (t0.hour * 3600 + t0.minute * 60 + t0.second) / 86400.
    dtai = erfa.dat(t0.year, t0.month, t0.day, frac_day)

    # Convert time-related parameters to the appropriate units and 1-dim shape
    mjd = times.mjd
    ra = source.ra.rad.ravel()
    dec = source.dec.rad.ravel()
    dut1 = delta_ut1_utc.to_value(u.s)
    pm_x = polar_motion_x.to_value(u.arcsec)
    pm_y = polar_motion_y.to_value(u.arcsec)

    geometric, dry, wet = lowlevel.calc(
        ref_x, ref_y, ref_z, ants_x, ants_y, ants_z,
        temperature, pressure, relative_humidity,
        mjd, ra, dec, pm_x, pm_y, dut1, dtai, axis_offset=axis_offset)
    delay = (geometric + dry + wet) * u.s
    delay = delay.reshape(original_times_shape + original_ants_shape)
    return delay.item() if delay.ndim == 0 else delay
