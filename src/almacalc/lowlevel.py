
"""Low-level interface to Calc based on NumPy."""
import os

import numpy as np

try:
    from importlib.resources import path as resource_path
except ImportError:
    # Revert to backported package for Python < 3.7
    from importlib_resources import path as resource_path

from . import _calc11


def _valid_ephemeris(path):
    """Convert `path` to validated ephemeris filename (use internal one if None)."""
    if path is None:
        # Use JPL ephemeris file shipped with package (and ensure it's not a temp file)
        with resource_path('almacalc.data', 'DE421_little_Endian') as path:
            pass
    filename = str(path)
    if len(filename) > 128:
        raise ValueError('Ephemeris filename {!r} has > 128 characters'.format(filename))
    # At least check that the file exists to avoid a nasty crash
    if not os.path.isfile(filename):
        raise ValueError('Could not find ephemeris file {!r}'.format(filename))
    return filename


_BUILTIN_EPHEMERIS_FILENAME = ''


def calc(ref_x, ref_y, ref_z, ant_x, ant_y, ant_z,
         temperature, pressure, relative_humidity, mjd, ra, dec,
         polar_motion_x, polar_motion_y, delta_ut1_utc, delta_tai_utc,
         is_solar_system_object=False, axis_offset=0., ephemeris_path=None):
    """Calculate delays towards (ra, dec) between antennas and reference point.

    Parameters
    ----------
    ref_x, ref_y, ref_z : float
        Geocentric (ITRF) position of the array reference point, in metres
    ant_x, ant_y, ant_z : float or array of float, shape (M,)
        Geocentric (ITRF) position of each antenna, in metres
    temperature, pressure, relative_humidity : float or array of float, shape (M,)
        Temperature (deg C), pressure (hPa/mbar) and humidity (0-1) at each antenna
    mjd : float or array of float, shape (N,)
        The times when to compute the delays, in UTC Modified Julian Days (MJD)
    ra, dec : float or array of float, shape (N,)
        ICRS source position for each time slot, in radians
    polar_motion_x, polar_motion_y, delta_ut1_utc : float or array of float, shape (N,)
        Earth orientation parameters at each time (arcsec, arcsec, sec)
    delta_tai_utc : float
        Difference between TAI and UTC at epoch `mjd[0]` (aka # of leap seconds)
    is_solar_system_object : bool or array of bool, shape (N,), optional
        True if the source is a Solar System object (default is False for all times).
        This turns off gravitational delays for these objects.
    axis_offset : float or array of float, shape (M,), optional
        Axis offsets for each antenna, in metres (default is 0.0 for all antennas)
    ephemeris_path : str or :class:`~pathlib.Path`, optional
        Path name of JPL DE421 ephemeris, 128 chars max (use built-in one by default).
        The particular flavour expected by CALC is a "machine-dependent binary file"
        (not HORIZONS or SPICE/SPK/bsp), available via https://ssd.jpl.nasa.gov/.

    Returns
    -------
    geometric_delay : array of float, shape (N, M)
        Geometric delay per time slot between each antenna and reference, in seconds
    dry_delay : array of float, shape (N, M)
        Hydrostatic "dry" delay due to induced dipoles in tropospheric gases, in seconds
    wet_delay : array of float, shape (N, M)
        "Wet" delay due to permanent dipole of water vapour molecules, in seconds
    uvw : array of float, shape (3, N, M)
        projected telecope coordinates in metres
    """
    global _BUILTIN_EPHEMERIS_FILENAME  # noqa: W0603

    if ephemeris_path is None:
        if not _BUILTIN_EPHEMERIS_FILENAME:
            _BUILTIN_EPHEMERIS_FILENAME = _valid_ephemeris(None)
        ephemeris_filename = _BUILTIN_EPHEMERIS_FILENAME
    else:
        ephemeris_filename = _valid_ephemeris(ephemeris_path)

    # Ensure that all arrays will be at least 1-dimensional
    ant_x = np.ravel(ant_x)
    mjd = np.ravel(mjd)
    # Normalise all antenna-like arrays of shape (M,)
    ant_x, ant_y, ant_z, temperature, pressure, relative_humidity, axis_offset = \
        np.broadcast_arrays(ant_x, ant_y, ant_z,
                            temperature, pressure, relative_humidity, axis_offset)
    # Normalise all time-like arrays of shape (N,)
    mjd, ra, dec, polar_motion_x, polar_motion_y, \
        delta_ut1_utc, is_solar_system_object = np.broadcast_arrays(
            mjd, ra, dec, polar_motion_x, polar_motion_y,
            delta_ut1_utc, is_solar_system_object)

    # Work around Calc's ATMP, which replaces negative or zero pressure
    # with an average value based on the site height. Replace zero pressure
    # with a tiny value that effectively disables dry delays instead.
    pressure = np.maximum(pressure, 1e-20)
    # Work around limitation of Calc that expects more than one time slot.
    # On line 100 of almacalc.f the time derivatives of the EOPs (dUT1, PM_x and PM_y)
    # are estimated by differencing the first and last timestamp,
    # and NaNs happen if this difference is zero.
    single_time = (len(mjd) == 1)
    if single_time:
        # Add an extra time slot one second into the future (otherwise we divide by zero)
        mjd = np.r_[mjd, mjd[0] + 1 / 86400.]
        ra = np.r_[ra, ra[0]]
        dec = np.r_[dec, dec[0]]
        polar_motion_x = np.r_[polar_motion_x, polar_motion_x[0]]
        polar_motion_y = np.r_[polar_motion_y, polar_motion_y[0]]
        delta_ut1_utc = np.r_[delta_ut1_utc, delta_ut1_utc[0]]
        is_solar_system_object = np.r_[is_solar_system_object, is_solar_system_object[0]]

    # Source names are empty strings for now, since they are not used
    sourcename_arr = np.zeros((len(mjd), 8), dtype='S1')
    # XXX When source names are eventually supported, copy strings into char array
    # for i in range(n_times):
    #     sourcename_arr[i] = list(sourcename[i][:8].ljust(8, '\00'))

    # The main event
    geometric_delay, dry_delay, wet_delay, uvw = _calc11.almacalc(
        ref_x, ref_y, ref_z, ant_x, ant_y, ant_z, temperature, pressure, relative_humidity,
        mjd, ra, dec, is_solar_system_object, polar_motion_x, polar_motion_y,
        delta_ut1_utc, delta_tai_utc, axis_offset, sourcename_arr, ephemeris_filename)

    if single_time:
        # Remove the extra time slot again to complete the workaround
        geometric_delay = geometric_delay[:-1]
        dry_delay = dry_delay[:-1]
        wet_delay = wet_delay[:-1]
        uvw = uvw[:-1]

    return geometric_delay, dry_delay, wet_delay, uvw


def sastd(pressure, latitude, height):
    """Zenith delay at site due to "dry" (hydrostatic) component of atmosphere.

    Parameters
    ----------
    pressure : float or array
        Total barometric pressure at site, in hectopascal (hPa) or millibars
    latitude : float or array
        Geodetic latitude of site, in radians
    height : float or array
        Height of site above the geoid, in metres

    Returns
    -------
    delay : float or array
        Zenith delay as an excess path length, in metres
    """
    _sastd = np.vectorize(_calc11.sastd)
    zd, _, _ = _sastd(pressure, 0.0, latitude, height, 0.0)
    return zd.item() if zd.ndim == 0 else zd


def sastw(relative_humidity, temperature):
    """Zenith delay at site due to "wet" (non-hydrostatic) component of atmosphere.

    Parameters
    ----------
    relative_humidity : float or array
        Relative humidity at site, as a fraction in range [0, 1]
    temperature : float or array
        Ambient air temperature at site, in degrees Celsius

    Returns
    -------
    delay : float or array
        Zenith delay as an excess path length, in metres
    """
    _sastw = np.vectorize(_calc11.sastw)
    zw, _ = _sastw(relative_humidity, temperature, 0.0, 0.0)
    return zw.item() if zw.ndim == 0 else zw


def gmf11(epoch, latitude, longitude, height, elevation):
    """Elevation dependence of tropospheric delay via Global Mapping Function.

    Parameters
    ----------
    epoch: float or array
        Time of observation, as (unmodified) Julian Days
    latitude, longitude : float or array
        Geodetic latitude and longitude of site, in radians
    height : float or array
        Height of site above the geoid, in metres
    elevation : float or array
        Elevation angle, in radians

    Returns
    -------
    gmfh, gmfw : float or array
        Hydrostatic and wet mapping function outputs, which are unitless numbers
        akin to airmass that scale zenith delays for the requested elevations.
    """
    def __gmf11(epoch, dlat, dlon, dhgt, el):
        """Strip off partial derivative output of GMF11."""
        mfh, mfw = _calc11.gmf11(epoch, dlat, dlon, dhgt, el)
        return mfh[0], mfw[0]
    _gmf11 = np.vectorize(__gmf11)
    gmfh, gmfw = _gmf11(epoch, latitude, longitude, height, elevation)
    if gmfh.ndim == 0 and gmfw.ndim == 0:
        return gmfh.item(), gmfw.item()
    else:
        return gmfh, gmfw
