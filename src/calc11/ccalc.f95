!
! create an interface that is easy to call from C - there are two areas to be careful of
!
!  MULTIDIMENSIONAL ARRAYS
!
!   Where multidimensional arrays are used in the Fortran is column major and C is
!   row major - which means that, in the linear memory layout, for Fortran the first index varies
!   fastest and for C the last index varies fastest.
!
!   A(n,m,p) <=> a[p][m][n]
!
!   When accessing element A(i,j,k) in Fortran the equivalent in C is a[k-1][j-1][i-1]
!   (Fortran begins indexing at one; C at zero)
!
! CHARACTER ARRAYS
!
! The fixed length nature of fortran strings that are space padded compared
! with C strings that are variable length terminated by null
! In addition arrays of strings i.e. C declaration char *c[] need to be dealt with
! as arrays of pointers
!
!
!

module c_calc_integration
implicit none

private

public :: ccalc

contains

function convert_c_string(inchar, n) result(res)
  use iso_c_binding, only: C_CHAR, c_null_char
    character(kind=c_char), intent(in) :: inchar(:)
    integer, intent(in) :: n
    character(n):: res
    integer :: i

    res=' '
    loop_string: do i=1, n
      if ( inchar(i) == c_null_char ) then
         exit loop_string
      else
         res(i:i) = inchar(i)
      end if
   end do loop_string

end function

subroutine ccalc(refx,refy,refz,nant,antx,anty,antz,temp,pressure,humidity,      &
                   ntimes,mjd,ra,dec,ssobj,dx,dy,dut,leapsec,axisoff,            &
                   sourcenamein,jpx_de421_in,geodelay,drydelay,wetdelay,uvwproj) &
                   bind (C, name="ccalc")
      use ISO_C_BINDING

!inputs
      real*8, value :: refx,refy,refz
      integer*4, value :: nant
      real*8, dimension(nant),intent(in) :: antx, anty, antz
      real*8, dimension(nant),intent(in) :: temp
      real*8, dimension(nant),intent(in) :: pressure
      real*8, dimension(nant),intent(in) :: humidity
      integer*4,value :: ntimes
      real*8, dimension(ntimes),intent(in) :: mjd
      real*8, dimension(ntimes),intent(in) :: ra,dec
      logical(c_bool), dimension(ntimes),intent(in) :: ssobj
      real*8, dimension(ntimes),intent(in) :: dx,dy
      real*8, dimension(ntimes),intent(in) :: dut
      real*8, value :: leapsec
      real*8, dimension(nant),intent(in) :: axisoff
      !forced into character*1 arrays below by compatibility
      type (c_ptr), dimension(ntimes),intent(in) :: sourcenamein
      character (kind=c_char, len=1), dimension(129),intent(in) :: jpx_de421_in
      real*8, dimension(ntimes,nant),intent(out) :: geodelay, drydelay, wetdelay
      real*8, dimension(3,ntimes,nant),intent(out) :: uvwproj
!locals
      character (len=8), dimension(ntimes) :: sourcename
      character (len=128) :: JPX_DE421
      character(kind=c_char), pointer :: fptr(:)
      logical, dimension(ntimes) ::  slogical

      integer :: i,j

      JPX_DE421 = convert_c_string(jpx_de421_in,128)

      do j = 1, ntimes
         call c_f_pointer(sourcenamein(j), fptr, [12])
         sourcename(j) = convert_c_string(fptr,8)
         slogical(j) = ssobj(j) ! need to copy to plain logical, otherwise becomes true when passed to almacalc
      enddo

      call almacalc(refx, refy, refz,                                  &
                         nant, antx, anty, antz,                       &
                         temp, pressure, humidity,                     &
                         ntimes, mjd, ra, dec, slogical,               &
                         dx, dy, dut, leapsec, axisoff, sourcename,    &
                         JPX_DE421,                                    &
                         geodelay, drydelay, wetdelay, uvwproj)

end subroutine ccalc

end module
