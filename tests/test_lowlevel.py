import pytest
import numpy as np

from almacalc import lowlevel


def test_calc():
    # 1 antenna, 2 times
    ref_x, ref_y, ref_z = 1000.0, 1299.0, 700.0
    ant_x, ant_y, ant_z = -3101.52, -11245.77, 8916.26
    temp, pressure, relative_humidity = 30.0, 1000.0, 0.7
    mjd2 = np.array([58701.45833333334, 58701.49930555555])
    ra, dec = 2.0, 1.0
    dut1, pm_x, pm_y = 0.0, 0.0, 0.0
    leapsec = 37.0
    # These values are taken from test_almacalc.out
    geo_expected = np.array([[-1.6456996715386752e-5], [-2.2521328279609168e-5]])
    dry_expected = np.array([[3.1142312389072535e-9], [3.0580114084074736e-9]])
    wet_expected = np.array([[1.1857223257447686e-9], [1.1411305051149612e-9]])

    geo_delay, dry_delay, wet_delay, uvw = lowlevel.calc(
        ref_x, ref_y, ref_z, ant_x, ant_y, ant_z, temp, pressure, relative_humidity,
        mjd2, ra, dec, pm_x, pm_y, dut1, leapsec)

    np.testing.assert_allclose(geo_delay, geo_expected, rtol=1e-14)
    np.testing.assert_allclose(dry_delay, dry_expected, rtol=1e-14)
    np.testing.assert_allclose(wet_delay, wet_expected, rtol=1e-14)

    # 2 antennas (because of extra weather measurement), 2 times
    relative_humidity2 = [0.7, 0.35]
    geo_delay, dry_delay, wet_delay, uvw = lowlevel.calc(
        ref_x, ref_y, ref_z, ant_x, ant_y, ant_z, temp, pressure, relative_humidity2,
        mjd2, ra, dec, pm_x, pm_y, dut1, leapsec)

    np.testing.assert_allclose(geo_delay, np.c_[geo_expected, geo_expected], rtol=1e-14)
    np.testing.assert_allclose(dry_delay, np.c_[dry_expected, dry_expected], rtol=1e-14)
    np.testing.assert_allclose(wet_delay, np.c_[wet_expected, 0.5 * wet_expected], rtol=1e-14)

    # 1 antenna, 1 time (check workaround)
    mjd1 = mjd2[0]
    geo_delay, dry_delay, wet_delay, uvw = lowlevel.calc(
        ref_x, ref_y, ref_z, ant_x, ant_y, ant_z, temp, pressure, relative_humidity,
        mjd1, ra, dec, pm_x, pm_y, dut1, leapsec)

    np.testing.assert_allclose(geo_delay, geo_expected[:-1], rtol=1e-14)
    np.testing.assert_allclose(dry_delay, dry_expected[:-1], rtol=1e-14)
    np.testing.assert_allclose(wet_delay, wet_expected[:-1], rtol=1e-14)

    # 1 antenna, 1 time, and disable tropospheric delays
    geo_delay, dry_delay, wet_delay, uvw = lowlevel.calc(
        ref_x, ref_y, ref_z, ant_x, ant_y, ant_z, temp, 0.0, 0.0,
        mjd1, ra, dec, pm_x, pm_y, dut1, leapsec)

    np.testing.assert_allclose(geo_delay, geo_expected[:-1], rtol=1e-14)
    np.testing.assert_allclose(dry_delay, 0.0, atol=1e-30)
    np.testing.assert_allclose(wet_delay, 0.0, atol=1e-30)

    # Unable to broadcast arrays due to mismatching shapes
    ant_x3 = np.r_[ant_x, ant_x, ant_x]
    ant_y2 = np.r_[ant_y, ant_y]
    with pytest.raises(ValueError):
        lowlevel.calc(ref_x, ref_y, ref_z, ant_x3, ant_y2, ant_z,
                      temp, pressure, relative_humidity,
                      mjd2, ra, dec, pm_x, pm_y, dut1, leapsec)
    ra3 = [1.0, 2.0, 3.0]
    with pytest.raises(ValueError):
        lowlevel.calc(ref_x, ref_y, ref_z, ant_x, ant_y, ant_z,
                      temp, pressure, relative_humidity,
                      mjd2, ra3, dec, pm_x, pm_y, dut1, leapsec)
