import sys

if sys.version_info.major < 3:
    # Skip high-level interface because it has Python 3 syntax
    collect_ignore = ['test_highlevel.py']
