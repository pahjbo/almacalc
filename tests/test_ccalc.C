/*
 * Test calling almacalc from C++/C
 *
 * using emerlin telescopes as examples, to match an emerlin test case.
 *
 */

#include <string>
#include <stdexcept>

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <map>
#include "stdint.h"
#include "ccalc.h"



struct telinfo {
      std::string vlbiname; //should be max 8 characters long
      double x,y,z;
      double axis_offset;
};




int main (int argc, char** argv) {

   //Pickmere telescope listed as TABLEY in ocean_*.coef files (telescope located in a farmers field
   //between the villages of Pickmere and Tabley in the NW of England).

   std::map<std::string,telinfo> tels = {
                     {"lov",{"JODLMKI ", 3822252.643, -153995.683, 5086051.443, 0.0  }},
                     {"mk2",{"JB_Mk_2 ", 3822473.365, -153692.318, 5085851.303, 0.458}},
                     {"cam",{"CAMB32  ", 3919982.752,    2651.982, 5013849.826, 1.503}},
                     {"dar",{"DARNHALL", 3828714.513, -169458.995, 5080647.749, 0.0  }},
                     {"def",{"DEFFORD ", 3923069.171, -146804.368, 5009320.528, 1.901}},
                     {"kno",{"KNOCKIN ", 3859711.503, -201995.077, 5056134.251, 0.0  }},
                     {"pic",{"TABLEY  ", 3817176.561, -162921.179, 5089462.057, 0.0  }}
   };

    if (argc < 3) {
        printf("Usage: calc11 <tel_name_1> <tel_name_2> [pressure temperature humidity] \n");
        printf("pressure in mbar, temperature in C, humidity as fraction\n");
    	return -1;
    }

    auto tel1 = tels.at(argv[1]);
    auto tel2 = tels.at(argv[2]);
    const char * telescope_names[] = {argv[1],argv[2]};

    double t=20.0,p=1015,rh=0.5; //default values
    if (argc > 3) {
        if (argc < 6) {
            printf("Usage: calc11 <tel_name_1> <tel_name_2> [pressure temperature humidity] \n");
            printf("pressure in mbar, temperature in C, humidity as fraction\n");
            return -1;
        } else {
           //use the "supplied" met data
            p = atof(argv[3]);
            t = atof(argv[4]);
            rh = atof(argv[5]);
        }
    }
    const int nant=2;
    double mjd[] = {59155.0, 59156.0, 59157.0},leapsec = 37;
    double temp[]={t,t}, pressure[]={p,p}, humidity[]={rh,rh};

    const int ntimes=3;
    const double dut[] = { -0.17292238,
                     -0.17292234,
                     -0.17292229};
    double ra[ntimes],dec[ntimes];
    const char * sourcename[ntimes];
    bool ssobj[ntimes];
    double dx[] = {0.15630551, 0.15630536, 0.15630521};
    double dy[] = {0.29504742, 0.29504742, 0.29504729};

    for(int i=0; i < ntimes; ++i)
    {
      ra[i] = 0.8718;
      dec[i] = 0.72452;
      sourcename[i]="DUMMY";
      ssobj[i] = false;

    }
    const char* calc11_root = std::getenv("CALC11_DATA");

    if (calc11_root == nullptr) {
        throw std::runtime_error("calc11 data path not set [CALC11_DATA not set in environment]");
    }

    //set the root path to the files
    std::string tilt_path(calc11_root);
    std::string de421_path(calc11_root);
    std::string ocean_path(calc11_root);
    std::string pole_path(calc11_root);

    //append file names to the path
    tilt_path += "tilt.dat";
    de421_path += "DE421_little_Endian";
    ocean_path += "ocean_load.coef";
    pole_path += "ocean_pole_tide.coef";


    const double antx[] = {tel1.x, tel2.x};
    const double anty[] = {tel1.y, tel2.y};
    const double antz[] = {tel1.z, tel2.z};
    const double axisoff[] = {tel1.axis_offset, tel2.axis_offset};

    double geodelay[nant][ntimes], drydelay[nant][ntimes], wetdelay[nant][ntimes], uvwproj[nant][ntimes][3];

    double refx=0,refy=0,refz=0;
    ccalc(refx, refy, refz,
                             nant, antx, anty, antz,
                             temp, pressure, humidity,
                             ntimes, mjd, ra, dec, ssobj,
                             dx, dy, dut, leapsec, axisoff, sourcename,
                             de421_path.c_str(),
                             &geodelay[0][0], &drydelay[0][0], &wetdelay[0][0], &uvwproj[0][0][0]);

    double delays_1[ntimes], delays_2[ntimes];

    for(int i=0; i < ntimes; ++i)
    {
      delays_1[i] = geodelay[0][i]+drydelay[0][i]+drydelay[0][i];
      delays_2[i] = geodelay[1][i]+drydelay[1][i]+drydelay[1][i];

    }
    const int ti=1;

    printf("telescope 1     delay = %17.10e secs, telescope 2 delay = %17.10e secs\n", delays_1[ti], delays_2[ti]);
    printf("telescope 1 dry delay = %17.10e secs, telescope 2 delay = %17.10e secs\n", drydelay[0][ti], drydelay[1][ti]);
    printf("telescope 1 wet delay = %17.10e secs, telescope 2 delay = %17.10e secs\n", wetdelay[0][ti], wetdelay[1][ti]);
    printf("Baseline delay difference: %s -> %s = %17.10e secs\n",
    		telescope_names[0], telescope_names[1], fabs(delays_1[ti] - delays_2[ti]));

    //extract the values from the output block for telescope 1
    double u1 = uvwproj[0][ti][0];
    double v1 = uvwproj[0][ti][1];
    double w1 = uvwproj[0][ti][2];

    printf("%s: UVW output %f, %f, %f\n",
    	telescope_names[0], u1, v1, w1);

    //extract the values from the output block for telescope 2
    double u2 = uvwproj[1][ti][0];
    double v2 = uvwproj[1][ti][1];
    double w2 = uvwproj[1][ti][2];
    printf("%s: UVW output %f, %f, %f\n",
        telescope_names[1], u2, v2, w2);

    printf("%s -> %s Baseline uvw: %f, %f, %f\n",
        telescope_names[0], telescope_names[1], u1 - u2, v1 - v2, w1 -w2);

    return 0;
}




