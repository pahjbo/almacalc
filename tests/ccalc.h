/*!
 * @file ccalc.h
 *
 * @date 19 Feb 2021
 * @author Paul Harrison
 * @version $Revision: 1 $
 * @copyright Paul Harrison, The University of Manchester 2021 
 */
#ifndef CCALC_H_
#define CCALC_H_

//we don't want C++ name mangling functions
#ifdef __cplusplus
extern "C" {
#endif
/*!
 * calculate delays and projected baselines for an interferometer.
 *
 *
 * @param refx  x coordinate (m) of Geocentric position of the array reference point (ITRF)
 * @param refy  y coordinate (m) of Geocentric position of the array reference point (ITRF)
 * @param refz  z coordinate (m) of Geocentric position of the array reference point (ITRF)
 * @param nant  number of antennae to calculate delays for.
 * @param antx  [nant] x coordinate (m) of Geocentric position of each antenna
 * @param anty  [nant] y coordinate (m) of Geocentric position of each  antenna
 * @param antz  [nant] z coordinate (m) of Geocentric position of each antenna
 * @param temp  [nant] temperature (deg. C) at each antenna
 * @param pressure  [nant] Atmospheric pressure (hPa) at each antenna
 * @param humidity [nant]  humidity (0-1) at each antenna
 * @param ntimes Number of time slots to compute delays
 * @param mjd [ntimes] the MJDs at which the delays should be calculated (note ntimes must be greater than one because of calc internal interpolation)
 * @param ra  [ntimes] ICRS Source right ascension (radians) for each time slot
 * @param dec  [ntimes] ICRS Source declination (radians) for each time slot
 * @param ssobj [ntimes] True if the source is a solar system object.
 * @param dx [ntimes] EOP celestial pole offset (arc-sec)
 * @param dy [ntimes]  EOP celestial pole offset (arc-sec)
 * @param dut [ntimes] UT1-UTC (sec)
 * @param leapsec number of leap seconds
 * @param axisoff [nant] axis offsets (meters) for each antenna
 * @param sourcename [ntimes] source names, for future use with solar system objects
 * @param JPX_DE421 Path name of the JPL ephemeris
 * @param[out] geodelay [nant][ntimes] Geometric delays (s) for each antenna at each timeslot
 * @param[out] drydelay [nant][ntimes] Dry delay for each antenna at each timeslot
 * @param[out] wetdelay [nant][ntimes] Wet delay for each antenna at each timeslot
 * @param[out] uvwproj  [nant][ntimes][3] UVW (m) projection for each antenna at each timeslot
 *
 * note that the memory for the output parameters needs to be allocated by the caller so
 * for instance the following declaration for geodelay would be acceptable
 *
 *     double geodelay[nant][ntimes]
 *
 * then it should be passed as an argument with
 *
 *     &geodelay[0][0]
 */
extern void ccalc(const double refx, const double refy, const double refz,
                      const int nant, const double antx[], const double anty[], const double antz[],
                      const double temp[], const double pressure[], const double humidity[],
                      const int ntimes, const double mjd[], const double ra[],const double dec[],
                      const bool ssobj[], const double dx[], const double dy[], const double dut[],
                      const double leapsec, const double axisoff[],
//                            length 8              128
                      const char *sourcename[], const char * JPX_DE421,
                      double *geodelay, double *drydelay, double *wetdelay, double *uvwproj);
#ifdef __cplusplus
}
#endif




#endif /* CCALC_H_ */
